<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $page = [
            'key' => 'about-company',
            'title' => 'About our company',
            'short_description' => 'About our company. We are great company',
            'description' => 'About our company. We are great company.'
        ];

        $_page = \App\Models\Page::where('key', $page['key'])->first();

        if (!$_page) {
            DB::table('pages')->insert([
                $page
            ]);
        }
    }
}
