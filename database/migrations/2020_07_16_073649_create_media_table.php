<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media', function (Blueprint $table) {
            $table->id();
            $table->boolean('main_image')->nullable();
            $table->string('client_file_name')->nullable();
            $table->string('original_file_name')->nullable();
            $table->string('path')->nullable();
            $table->json('conversions')->nullable();
            $table->unsignedBigInteger('size')->nullable();
            $table->string('mime', 127)->nullable();
            $table->timestamps();

            $table->index('imageable_id');
            $table->index('imageable_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media');
    }
}
