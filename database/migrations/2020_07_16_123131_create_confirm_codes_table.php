<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfirmCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('confirm_codes', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->integer('expire');
            $table->unsignedInteger('user_id');
            $table->enum('used', ['0', '1'])->default('0');
            $table->timestamps();

            $table->index('code');
            $table->index('used');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('confirm_codes');
    }
}
