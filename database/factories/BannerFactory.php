<?php

use App\Models\Banner;
use Illuminate\Database\Eloquent\Factory;
use Faker\Generator as Faker;


/** @var Factory $factory */

$factory->define(Banner::class, function (Faker $faker) {
    return [
        'title' => $faker->word,
        'description' => $faker->paragraph,
        'position' => 1
    ];
});

$factory->state(Banner::class, 'active', function () {
    return [
        'status' => 'active'
    ];
});

$factory->state(Banner::class, 'notActive', function () {
    return [
        'status' => 'notActive'
    ];
});
