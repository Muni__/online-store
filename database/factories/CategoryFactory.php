<?php


/** @var Factory $factory */

use App\Model\Category;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;


$factory->define(Category::class, function (Faker $faker) {

    return [
        'name' => $faker->jobTitle,
        'category_id' => null,
        'model' => \App\Model\Product::class,
    ];
});
