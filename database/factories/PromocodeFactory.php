<?php


/** @var Factory $factory */

use App\Model\Discount;
use App\Models\Promocode;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;


$factory->define(Promocode::class, function (Faker $faker) {
    return [
        'title' => 'Example discount 10%',
        'description' => 'Example discount 10%',
        'code' => 'CD12345',
        'type' => 'disposable',
        'discount_id' => function(){
            return factory(Discount::class)->create()->id;
        },
    ];
});
