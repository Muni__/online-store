<?php


/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Discount;
use Faker\Generator as Faker;


$factory->define(Discount::class, function (Faker $faker) {
    return [
        'title' => 'Example discount 10%',
        'description' => 'Example discount 10%',
        'type' => 'percent',
        'discount' => 10,
    ];
});
