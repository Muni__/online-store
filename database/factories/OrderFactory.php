<?php


/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Order;
use App\Models\User;
use Faker\Generator as Faker;


$factory->define(Order::class, function (Faker $faker) {
    return [
        'user_id' => function() {
            return factory(User::class)->create()->id;
        },
        'amount' => 50000,
        'delivery_address' => $faker->address,
        'payment_status' => 'pending'
    ];
});
