<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Product;
use App\Model\Category;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Models Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Product::class, function (Faker $faker) {
    return [
        'title' => $faker->word,
        'short_description' => $faker->sentence,
        'description' => $faker->paragraph,
        'category_id' => function() {
            return factory(Category::class)->create()->id;
        },
        'price' => 10000,
    ];
});

$factory->state(Product::class, 'active', function () {
    return [
        'status' => 'active'
    ];
});

$factory->state(Product::class, 'notActive', function () {
    return [
        'status' => 'notActive'
    ];
});
