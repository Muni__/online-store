<?php

use App\Model\Category;
use App\Models\Blog;
use Illuminate\Database\Eloquent\Factory;
use Faker\Generator as Faker;


/** @var Factory $factory */

$factory->define(Blog::class, function (Faker $faker) {
    return [
        'title' => $faker->word,
        'short_description' => $faker->sentence,
        'description' => $faker->paragraph,
        'category_id' => function() {
            return factory(Category::class)->create()->id;
        },
    ];
});

$factory->state(Blog::class, 'active', function () {
   return [
       'status' => 'active'
   ];
});

$factory->state(Blog::class, 'notActive', function () {
    return [
        'status' => 'notActive'
    ];
});
