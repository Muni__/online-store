<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'contacts'], function () {
    Route::get('/', 'ContactController@list');
});

Route::group(['prefix' => 'products'], function () {
    Route::get('/', 'ProductController@list');
    Route::get('/{product_id}', 'ProductController@item');
});

Route::group(['prefix' => 'categories'], function () {
    Route::group(['prefix' => 'product'], function () {
        Route::get('/', 'CategoryController@productCategories');
        Route::get('/{category_slug}', 'ProductController@getByCategory');
    });

    Route::group(['prefix' => 'news'], function () {
        Route::get('/', 'CategoryController@blogCategories');
        Route::get('/{category_slug}', 'BlogController@getByCategory');
    });
});


Route::group(['prefix' => 'news'], function () {
    Route::get('/', 'BlogController@list');
    Route::get('/{slug}', 'BlogController@item');
});

Route::group(['prefix' => 'pages'], function () {
    Route::get('/{key}', 'PageController@getByKey');
});

Route::group(['prefix' => 'banners'], function () {
    Route::get('/', 'BannerController@active_list');
});

Route::post('/check-promocode/{code}', 'PromoCodeController@check');

Route::group(['namespace' => 'Frontend'], function(){
    Route::group(['prefix' => 'orders', 'middleware' => 'auth:api'], function () {
        Route::get('/', 'PurchasesController@list');
        Route::post('/', 'PurchasesController@store');
    });

    Route::group(['prefix' => 'auth'], function () {
        Route::get('remind/{code}', 'Auth\LoginController@confirmCode');
        Route::post('remind/{code}', 'Auth\LoginController@changePassword');
        Route::post('register', 'Auth\RegisterController@register');
        Route::post('login', 'Auth\LoginController@login');
        Route::post('logout', 'Auth\LoginController@logout');
        Route::post('remind', 'Auth\LoginController@remind');
    });

    Route::group(['prefix' => 'profile', 'middleware' => 'auth:api'], function () {
        Route::get('/', 'ProfileController@list');
        Route::put('/', 'ProfileController@update');
        Route::put('/change-password', 'ProfileController@changePassword');
    });
});
Route::post('/admin/login', 'Backend\LoginController@login');

Route::group(['prefix' => 'admin', 'middleware' => ['auth:api', 'admin_accessible']], function(){

    Route::group(['namespace' => 'Backend'], function () {

        Route::post('/logout', 'LoginController@logout');

        Route::group(['prefix' => 'purchases'], function () {
            Route::get('/', 'PurchaseController@list');
            Route::get('/{order_id}', 'PurchaseController@item');
            Route::put('/{order_id}', 'PurchaseController@update');
            Route::delete('/{order_id}', 'PurchaseController@delete');
        });

    });

    Route::group(['prefix' => 'products'], function () {
        Route::get('/', 'ProductController@list');
        Route::get('/{product_id}', 'ProductController@item');
        Route::post('/', 'ProductController@store');
        Route::post('/status/{product_id}', 'ProductController@changeStatus');
        Route::put('/{product_id}', 'ProductController@update');
        Route::delete('/{product_id}', 'ProductController@delete');
        Route::post('/upload/{product_id}', 'ProductController@upload');
    });

    Route::group(['prefix' => 'promocodes'], function () {
        Route::get('/', 'PromoCodeController@list');
        Route::get('/{promocode_id}', 'PromoCodeController@item');
        Route::post('/', 'PromoCodeController@store');
        Route::put('/{promocode_id}', 'PromoCodeController@update');
        Route::delete('/{promocode_id}', 'PromoCodeController@delete');
    });

    Route::group(['prefix' => 'discounts'], function () {
        Route::get('/', 'DiscountController@list');
        Route::get('/{discount_id}', 'DiscountController@item');
        Route::post('/', 'DiscountController@store');
        Route::put('/{discount_id}', 'DiscountController@update');
        Route::delete('/{discount_id}', 'DiscountController@delete');
    });

    Route::group(['prefix' => 'banners'], function () {
        Route::get('/', 'BannerController@list');
        Route::get('/{banner_id}', 'BannerController@item');
        Route::post('/', 'BannerController@store');
        Route::post('/status/{banner_id}', 'BannerController@changeStatus');
        Route::put('/{banner_id}', 'BannerController@update');
        Route::delete('/{banner_id}', 'BannerController@delete');
        Route::post('/upload/{product_id}', 'BannerController@upload');

    });

});
