<?php

namespace Tests\Unit;

use App\Model\Order;
use App\Model\OrderItem;
use App\Model\Product;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class OrderTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function check_add_items_to_order() {
        $order = factory(Order::class)->create();

        $order_items = [
            [
                'product_id' => 1,
                'sold_price' => 1500,
                'count' => 3,
            ],
            [
                'product_id' => 2,
                'sold_price' => 1500,
                'count' => 3,
            ],
            [
                'product_id' => 3,
                'sold_price' => 1500,
                'count' => 3,
            ]
        ];

        $order->addItems($order_items);

        $order->load('order_items');

        $this->checkOrderItems($order, $order_items);
        $this->assertEquals(3, $order->order_items->count());
    }

    /**
     * @test
     */
    public function check_order_saved() {
        $orderData = [
            'amount' => 15000,
            'delivery_address' => 'Delivery address',
            'user_id' => 1
        ];

        $orderItemsData = [
                [
                    'product_id' => 1,
                    'sold_price' => 5000,
                    'count' => 1,
                ],
                [
                    'product_id' => 2,
                    'sold_price' => 5000,
                    'count' => 1,
                ],
                [
                    'product_id' => 3,
                    'sold_price' => 5000,
                    'count' => 1,
                ],
        ];

        $order = Order::saveOrder($orderData, $orderItemsData);

        $this->assertDatabaseHas('orders', $orderData);
        $this->checkOrderItems($order, $orderItemsData);
    }


    /**
     * @test
     */
    public function will_not_save_order_if_order_items_do_not_save() {

        $orderData = [
            'amount' => 15000,
            'delivery_address' => 'Delivery address',
            'user_id' => 1
        ];

        $orderItemsData = [
            [
                'wrong_product_id' => 1,
                'sold_price' => 5000,
                'count' => 1,
            ],
        ];

        try {
            Order::saveOrder($orderData, $orderItemsData);
        }catch (ModelNotFoundException $exception) {
            $this->assertDatabaseMissing('orders', [
                'delivery_address' => 'Delivery address',
                'amount' => 15000,
                'user_id' => 1
            ]);
            return;
        }

        $this->fail();
    }

    /**
     * @test
     */
    public function filterOrderByStatus() {
        $payedOrdersA = factory(Order::class)->create([
            'payment_status' => 'payed'
        ]);
        $payedOrdersB = factory(Order::class)->create([
            'payment_status' => 'payed'
        ]);
        $payedOrdersC = factory(Order::class)->create([
            'payment_status' => 'payed'
        ]);
        $pendingOrder = factory(Order::class)->create([
            'payment_status' => 'pending'
        ]);

        $orders = Order::filter(['status' => 'payed'])->get();

        $this->assertEquals($orders->count(), 3);
        $this->assertTrue($orders->contains($payedOrdersA));
        $this->assertTrue($orders->contains($payedOrdersB));
        $this->assertTrue($orders->contains($payedOrdersC));
        $this->assertFalse($orders->contains($pendingOrder));
    }

    /**
     * @test
     */
    public function filterOrderByEmail() {
        $userA = factory(User::class)->create();
        $userB = factory(User::class)->create();
        $payedOrdersA = factory(Order::class)->create([
            'user_id' => $userA->id
        ]);
        $payedOrdersB = factory(Order::class)->create([
            'user_id' => $userA->id
        ]);
        $payedOrdersC = factory(Order::class)->create([
            'user_id' => $userA->id
        ]);
        $pendingOrder = factory(Order::class)->create([
            'user_id' => $userB->id
        ]);

        $orders = Order::filter(['email' => $userA->email])->get();

        $this->assertEquals($orders->count(), 3);
        $this->assertTrue($orders->contains($payedOrdersA));
        $this->assertTrue($orders->contains($payedOrdersB));
        $this->assertTrue($orders->contains($payedOrdersC));
        $this->assertFalse($orders->contains($pendingOrder));
    }

    /**
     * @param $order
     * @param array $order_items
     */
    private function checkOrderItems($order, array $order_items) {
        foreach ($order_items as $order_item) {
            $this->assertDatabaseHas('order_items', [
                'order_id' => $order->id,
                'product_id' => $order_item['product_id'],
                'sold_price' => $order_item['sold_price'],
                'count' => $order_item['count'],
            ]);
        }
    }


}
