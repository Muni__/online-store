<?php

namespace Tests\Unit;

use App\Model\Product;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class ProductTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function get_only_active_products() {
        $activeProductA = factory(Product::class)->state('active')->create();
        $activeProductB = factory(Product::class)->state('active')->create();
        $notActiveProduct = factory(Product::class)->state('notActive')->create();

        $products = Product::active()->get();

        $this->assertTrue($products->contains($activeProductA));
        $this->assertTrue($products->contains($activeProductB));
        $this->assertFalse($products->contains($notActiveProduct));
    }
}
