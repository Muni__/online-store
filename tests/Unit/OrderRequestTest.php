<?php

namespace Tests\Unit;

use App\Http\Requests\OrderRequest;
use Faker\Factory;
use Tests\TestCase;

class OrderRequestTest extends TestCase
{
    /** @var OrderRequest */
    private $rules;

    private $validator;

    public function setUp(): void
    {
        parent::setUp();

        $this->validator = app()->make('validator');

        $this->rules = (new OrderRequest())->rules();
    }

    public function validationProvider() {
        $faker = Factory::create( Factory::DEFAULT_LOCALE);

        return [
            'request_should_fail_when_no_amount_is_provided' => [
                'passed' => false,
                'data' => [
                    'delivery_address' => 'example address',
                    'items' => [
                        [
                            'product_id' => 1,
                            'sold_price' => 5000,
                            'count' => 2
                        ],
                        [
                            'product_id' => 2,
                            'sold_price' => 7000,
                            'count' => 3
                        ]
                    ],
                ]
            ],
            'request_should_fail_when_no_delivery_address_is_provided' => [
                'passed' => false,
                'data' => [
                    'amount' => '8000',
                    'items' => [
                        [
                            'product_id' => 1,
                            'sold_price' => 5000,
                            'count' => 2
                        ],
                        [
                            'product_id' => 2,
                            'price' => 7000,
                            'count' => 3
                        ]
                    ],
                ]
            ],
            'request_should_fail_no_array_items_is_provided' => [
                'passed' => false,
                'data' => [
                    'amount' => '8000',
                    'delivery_address' => 'example address',
                    'items' => 'not_array'
                ]
            ],
            'request_should_fail_no_item_product_id_is_provided' => [
                'passed' => false,
                'data' => [
                    'amount' => '8000',
                    'delivery_address' => 'example address',
                    'items' => [
                        [
                            'sold_price' => 5000,
                            'count' => 2
                        ],
                        [
                            'sold_price' => 7000,
                            'count' => 3
                        ]
                    ],
                ]
            ],
            'request_should_fail_when_no_price_in_item_array_is_provided' => [
                'passed' => false,
                'data' => [
                    'amount' => '8000',
                    'delivery_address' => 'example address',
                    'items' => [
                        [
                            'product_id' => 1,
                            'count' => 2
                        ],
                        [
                            'product_id' => 2,
                            'count' => 3
                        ]
                    ],
                ]
            ],
            'request_should_fail_when_no_count_in_item_array_is_provided' => [
                'passed' => false,
                'data' => [
                    'amount' => '8000',
                    'delivery_address' => 'example address',
                    'items' => [
                        [
                            'product_id' => 1,
                            'sold_price' => 7000,
                        ],
                        [
                            'product_id' => 2,
                            'sold_price' => 7000,
                        ]
                    ],
                ]
            ],
            'request_should_pass' => [
                'passed' => true,
                'data' => [
                    'amount' => '8000',
                    'delivery_address' => 'example address',
                    'items' => [
                        [
                            'product_id' => 1,
                            'count' => 3,
                            'sold_price' => 7000,
                        ],
                        [
                            'product_id' => 2,
                            'count' => 3,
                            'sold_price' => 7000,
                        ]
                    ],
                ]
            ]
        ];
    }


    /**
     * @test
     * @dataProvider validationProvider
     * @param bool $shouldPass
     * @param array $mockedRequestData
     */

    public function validation_results_as_expected($shouldPass, $mockedRequestData)
    {
        $this->assertEquals(
            $shouldPass,
            $this->validate($mockedRequestData)
        );
    }

    protected function validate($mockedRequestData)
    {
        return $this->validator
            ->make($mockedRequestData, $this->rules)
            ->passes();
    }
}
