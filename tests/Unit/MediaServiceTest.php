<?php

namespace Tests\Unit;

use App\Model\Product;
use App\Services\MediaService\Facades\MediaService;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class MediaServiceTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * @test void
     */
    public function upload_file()
    {
        Storage::fake('media');
        $product = factory(Product::class)->state('active')->create();

        $media = MediaService::upload(UploadedFile::fake()->image('photo1.jpg'), get_class($product), $product->id);

        Storage::assertExists('public/media/'.$media->getNameBySize('xxs'));
        Storage::assertExists('public/media/'.$media->getNameBySize('xs'));
        Storage::assertExists('public/media/'.$media->getNameBySize('s'));
        Storage::assertExists('public/media/'.$media->getNameBySize('m'));
        Storage::assertExists('public/media/'.$media->getNameBySize('l'));
        Storage::assertExists('public/media/'.$media->getNameBySize('xl'));
        Storage::assertExists('public/media/'.$media->getNameBySize('xxl'));
        Storage::assertExists('public/media/'.$media->getNameBySize('3xl'));
    }

    /**
     * @test
     */
    public function when_deleting_model_it_should_deletes_all_files() {
        Storage::fake('media');

        $product = factory(Product::class)->state('active')->create();
        $media = MediaService::upload(UploadedFile::fake()->image('photo1.jpg'), get_class($product), $product->id);

        $media->delete();

        Storage::assertMissing('public/media/'.$media->getNameBySize('xxs'));
        Storage::assertMissing('public/media/'.$media->getNameBySize('xs'));
        Storage::assertMissing('public/media/'.$media->getNameBySize('s'));
        Storage::assertMissing('public/media/'.$media->getNameBySize('m'));
        Storage::assertMissing('public/media/'.$media->getNameBySize('l'));
        Storage::assertMissing('public/media/'.$media->getNameBySize('xl'));
        Storage::assertMissing('public/media/'.$media->getNameBySize('xxl'));
        Storage::assertMissing('public/media/'.$media->getNameBySize('3xl'));
    }
}
