<?php

namespace Tests\Unit;

use App\Model\Discount;
use App\Models\Promocode;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class PromoCodeTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * @test
     */
    public function check_function_used()
    {
        $promoCode = factory(Promocode::class)->make();
        $this->assertEquals($promoCode->number_of_uses, 0);
        $promoCode->used();
        $this->assertEquals($promoCode->number_of_uses, 1);
    }
}
