<?php

namespace Tests\Unit;

use App\Http\Middleware\AdminAccess;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Tests\TestCase;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AdminAccessMiddlewareTest extends TestCase
{
    /**
     * @test
     */
    public function user_does_not_have_access_if_he_does_not_have_admin_access() {
        $user = factory(User::class)->make([
            'role_id' => Role::USER
        ]);

        auth('api_admin')->login($user);

        $request = Request::create('/admin/logout', 'POST');

        $middleware = new AdminAccess();
        $this->expectException(HttpException::class);

        $middleware->handle($request, function () {});
    }
}
