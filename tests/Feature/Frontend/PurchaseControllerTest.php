<?php

namespace Tests\Feature\Frontend;

use App\Model\Discount;
use App\Model\Order;
use App\Model\Product;
use App\Models\Promocode;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class PurchaseControllerTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * @test
     * @return void
     */
    public function auth_customer_can_order_products()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->createOrder();

        $response->assertStatus(201);
        $response->assertJsonStructure([
            'id',
            'amount',
            'order_items' => [
                '*' => ['product_id', 'sold_price', 'count']
            ]
        ]);

        $order = Order::where('user_id', $user->id)->with('order_items')->first();

        $this->assertEquals(9*1500, $order->amount);
    }

    /**
     * @test
     */
    public function customer_cannot_create_order_if_it_is_not_auth() {
        $response = $this->createOrder();

        $response->assertStatus(401);
    }

    private function createOrder() {
        return $this->json('post', '/api/orders', [
            'delivery_address' => 'Street 123',
            'amount' => 9*1500,
            'items' => [
                [
                    'product_id' => 1,
                    'sold_price' => 1500,
                    'count' => 3,
                ],
                [
                    'product_id' => 2,
                    'sold_price' => 1500,
                    'count' => 3,
                ],
                [
                    'product_id' => 3,
                    'sold_price' => 1500,
                    'count' => 3,
                ]
            ]
        ]);
    }

    /**
     * @test
     */
    public function correct_purchasing_with_promocode()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $discount = factory(Discount::class)->create([
            'type' => 'percent',
            'discount' => 10
        ]);

        $promocode = factory(Promocode::class)->create([
            'title' => 'Test title',
            'description' => "Description about promocode",
            'code' => 'CD12345',
            'discount_id' => $discount->id
        ]);

        $response = $this->json('post', '/api/orders', [
            'delivery_address' => 'Street 123',
            'amount' => 9*1500*($promocode->discount->discount/100),
            'promocode_id' => $promocode->id,
            'items' => [
                [
                    'product_id' => 1,
                    'sold_price' => 1500,
                    'count' => 3,
                ],
                [
                    'product_id' => 2,
                    'sold_price' => 1500,
                    'count' => 3,
                ],
                [
                    'product_id' => 3,
                    'sold_price' => 1500,
                    'count' => 3,
                ]
            ]
        ]);

        $response->assertStatus(201);
        $response->assertJsonStructure([
            'id',
            'amount',
            'order_items' => [
                '*' => ['product_id', 'sold_price', 'count']
            ]
        ]);

        $order = Order::where('user_id', $user->id)->with('order_items', 'promocode')->first();
        $this->assertEquals(9*1500*($order->promocode->discount->discount/100), $order->amount);

        $promocode->refresh();
        $this->assertEquals($promocode->number_of_uses, 1);
    }

    /**
     * @test
     */
    public function get_all_auth_user_orders() {
        $user = factory(User::class)->create();
        $otherUser = factory(User::class)->create();
        $this->actingAs($user);

        $orderA = factory(Order::class)->create([
            'user_id' => $user->id
        ]);
        $orderB = factory(Order::class)->create([
            'user_id' => $user->id
        ]);
        $otherOrder = factory(Order::class)->create([
            'user_id' => $otherUser->id
        ]);

        $response = $this->json('get', '/api/orders');
        $response->assertStatus(200);
        $toArrayResponse = $response->original->items();

        $this->assertEquals(2, count($toArrayResponse));
        $this->assertTrue($orderA->is($toArrayResponse[0]));
        $this->assertTrue($orderB->is($toArrayResponse[1]));
    }
}
