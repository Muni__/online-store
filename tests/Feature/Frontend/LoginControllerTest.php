<?php

namespace Tests\Feature\Frontend;

use App\Mail\RemindPassword;
use App\Models\ConfirmCode;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Tests\TestCase;

class LoginControllerTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * @test
     */
    public function user_can_login()
    {
        factory(User::class)->create([
            'email' => 'aibek@example.com',
            'password' => Hash::make('password')
        ]);

        $loginData = [
            'email' => 'aibek@example.com',
            'password' => 'password'
        ];

        $response = $this->json('post','api/auth/login', $loginData);

        $response->assertStatus(200);
        $response->assertJsonStructure(['access_token', 'token_type', 'expires_in']);
    }

    /**
     * @test
     */
    public function user_can_not_login_if_email_wrong(){
        factory(User::class)->create([
            'email' => 'aibek@example.com',
            'password' => Hash::make('password')
        ]);

        $loginData = [
            'email' => 'wrong@example.com',
            'password' => 'password'
        ];

        $response = $this->json('post','api/auth/login', $loginData);

        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function user_can_not_login_if_password_wrong(){
        factory(User::class)->create([
            'email' => 'aibek@example.com',
            'password' => Hash::make('password')
        ]);

        $loginData = [
            'email' => 'aibek@example.com',
            'password' => 'wrong_password'
        ];

        $response = $this->json('post','api/auth/login', $loginData);

        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function user_can_not_login_if_email_are_missed(){
        factory(User::class)->create([
            'email' => 'aibek@example.com',
            'password' => Hash::make('password')
        ]);

        $loginData = [
            'password' => 'password'
        ];

        $response = $this->json('post','api/auth/login', $loginData);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['email']);
    }

    /**
     * @test
     */
    public function user_can_not_login_if_password_are_missed(){
        factory(User::class)->create([
            'email' => 'aibek@example.com',
            'password' => Hash::make('password')
        ]);

        $loginData = [
            'email' => 'aibek@example.com',
        ];

        $response = $this->json('post','api/auth/login', $loginData);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['password']);
    }

    /**
     * @test
     */
    public function user_can_logout() {
        $user = factory(User::class)->create();

        $credentials = [
            'email' => $user->email,
            'password' => 'password'
        ];

        auth('api')->attempt($credentials);

        $response = $this->json('post','api/auth/logout');
        $response->assertStatus(200);
        $response->assertJson([
            'message' => 'Successfully logged out',
        ]);
        $this->assertNull(Auth()->user());
    }

    /**
     * @test
     */
    public function user_can_get_email_if_he_forgot_password() {
        Mail::fake();

        $user = factory(User::class)->create();

        $response = $this->json('post', '/api/auth/remind', ['email' => $user->email, 'url' => 'https://localhost']);
        $response->assertStatus(200);
        $code = ConfirmCode::where(['user_id' => $user->id, 'used' => '0'])->first();

        Mail::assertQueued(RemindPassword::class, function ($mail) use ($user, $code) {
            return $mail->user->id === $user->id && $mail->confirmCode->code === $code->code;
        });
    }

    /**
     * @test
     */
    public function user_get_error_if_email_not_exist_for_reminding() {
        $response = $this->json('post', '/api/auth/remind', ['email' => 'fake@example.com', 'url' => 'https://localhost']);
        $response->assertStatus(404);
    }

    /**
     * @test
     */
    public function user_get_true_if_confirm_code_is_exist() {
        $code = $confirmCode = ConfirmCode::create([
            'code' => Str::random(20),
            'expire' => Carbon::now()->addHour(),
            'user_id' => 1,
        ]);

        $response = $this->json('get', '/api/auth/remind/'.$code->code);
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function user_get_true_if_confirm_code_is_not_exist() {
        $response = $this->json('get', '/api/auth/remind/wrong-code');
        $response->assertStatus(404);
    }

    /**
     * @test
     */
    public function user_get_error_if_confirm_code_was_expired() {
        $code = $confirmCode = ConfirmCode::create([
            'code' => Str::random(20),
            'expire' => Carbon::now()->subHour(),
            'user_id' => 1,
        ]);

        $response = $this->json('get', '/api/auth/remind/'.$code->code);
        $response->assertStatus(404);
    }

    /**
     * @test
     */
    public function user_can_change_password() {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();

        $code = $confirmCode = ConfirmCode::create([
            'code' => Str::random(20),
            'expire' => Carbon::now()->addHour(),
            'user_id' => $user->id,
        ]);

        $response = $this->json('post', '/api/auth/remind/'.$code->code, ['password' => 'newPassword']);
        $response->assertStatus(201);
        $user->refresh();
        $this->assertTrue(Hash::check('newPassword', $user->password));
    }
}
