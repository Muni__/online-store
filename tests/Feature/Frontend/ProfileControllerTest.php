<?php

namespace Tests\Feature\Frontend;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class ProfileControllerTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * @test
     */
    public function getInfoAboutUser()
    {
        $user = factory(User::class)->create([
            'email' => 'aibek@example.com',
            'first_name' => 'Aibek',
            'last_name' => 'Satarov',
            'phone' => '+996 999 99 99 99'
        ]);

        $this->actingAs($user);

        $response = $this->json('get', '/api/profile');

        $response->assertStatus(200);
        $response->assertJson([
            'email' => 'aibek@example.com',
            'first_name' => 'Aibek',
            'last_name' => 'Satarov',
            'phone' => '+996 999 99 99 99'
        ]);
    }

    /**
     * @test
     */
    public function editProfileData() {
        $user = factory(User::class)->create([
            'email' => 'aibek@example.com',
            'first_name' => 'Aibek',
            'last_name' => 'Satarov',
            'phone' => '+996 999 99 99 99'
        ]);

        $this->actingAs($user);

        $newData = [
            'first_name' => 'Ivan',
            'last_name' => 'Ivanov',
            'phone' => '+996 888 88 88 88'
        ];

        $response = $this->json('put', '/api/profile', $newData);
        $response->assertStatus(201);
        $user->refresh();
        $this->assertEquals('Ivan', $user->first_name);
        $this->assertEquals('Ivanov', $user->last_name);
        $this->assertEquals('+996 888 88 88 88', $user->phone);
    }

    /**
     * @test
     */
    public function email_does_not_change() {
        $user = factory(User::class)->create([
            'email' => 'aibek@example.com',
            'first_name' => 'Aibek',
            'last_name' => 'Satarov',
            'phone' => '+996 999 99 99 99'
        ]);

        $this->actingAs($user);

        $newData = [
            'email' => 'ivan@example.com',
            'first_name' => 'Ivan',
            'last_name' => 'Ivanov',
            'phone' => '+996 888 88 88 88'
        ];

        $response = $this->json('put', '/api/profile', $newData);
        $response->assertStatus(201);
        $user->refresh();
        $this->assertEquals('aibek@example.com', $user->email);
    }

    /**
     * @test
     */
    public function user_can_change_password() {
        $user = factory(User::class)->create([
            'password' => Hash::make('password'),
        ]);

        $this->actingAs($user);

        $newData = [
            'oldPassword' => 'password',
            'password' => 'newPassword',
        ];

        $response = $this->json('put', '/api/profile/change-password', $newData);
        $response->assertStatus(201);
        $user->refresh();
        $this->assertTrue(Hash::check('newPassword', $user->password));
    }

    /**
     * @test
     */
    public function user_get_error_if_first_name_is_empty() {
        $user = factory(User::class)->create([
            'email' => 'aibek@example.com',
            'first_name' => 'Aibek',
            'last_name' => 'Satarov',
            'phone' => '+996 999 99 99 99'
        ]);

        $this->actingAs($user);

        $newData = [
            'last_name' => 'Ivanov',
            'phone' => '+996 888 88 88 88'
        ];

        $response = $this->json('put', '/api/profile', $newData);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['first_name']);
    }

    /**
     * @test
     */
    public function user_get_error_if_last_name_is_empty() {
        $user = factory(User::class)->create([
            'email' => 'aibek@example.com',
            'first_name' => 'Aibek',
            'last_name' => 'Satarov',
            'phone' => '+996 999 99 99 99'
        ]);

        $this->actingAs($user);

        $newData = [
            'first_name' => 'Ivan',
            'phone' => '+996 888 88 88 88'
        ];

        $response = $this->json('put', '/api/profile', $newData);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['last_name']);
    }

    /**
     * @test
     */
    public function user_get_error_if_phone_is_empty() {
        $user = factory(User::class)->create([
            'email' => 'aibek@example.com',
            'first_name' => 'Aibek',
            'last_name' => 'Satarov',
            'phone' => '+996 999 99 99 99'
        ]);

        $this->actingAs($user);

        $newData = [
            'first_name' => 'Ivan',
            'last_name' => 'Ivanov',
        ];

        $response = $this->json('put', '/api/profile', $newData);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['phone']);
    }


}
