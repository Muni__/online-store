<?php

namespace Tests\Feature\Frontend;

use App\Mail\SuccessfulRegistration;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class RegisterControllerTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * @test
     */
    public function user_can_register()
    {
        $this->withoutExceptionHandling();
        Mail::fake();

        $register_data = [
            'first_name' => 'Aibek',
            'last_name' => 'Dastanov',
            'email' => 'aibek@example.com',
            'password' => 'password',
            'phone' => '+996 999 99 99 99'
        ];

        $response = $this->json('post', 'api/auth/register', $register_data);
        $response->assertStatus(201);
        $user = User::where('email', 'aibek@example.com')->first();
        $this->assertTrue(isset($user));

        Mail::assertQueued(SuccessfulRegistration::class, function ($mail) use ($user) {
            return $mail->user->id === $user->id;
        });
    }

    /**
     * @test
     */
    public function user_can_not_register_if_email_already_used() {
        Mail::fake();
        factory(User::class)->create([
            'email' => 'aibek@example.com'
        ]);

        $register_data = [
            'first_name' => 'Aibek',
            'last_name' => 'Dastanov',
            'email' => 'aibek@example.com',
            'password' => 'password',
        ];

        $response = $this->json('post', 'api/auth/register', $register_data);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['email']);
    }

    /**
     * @test
     */
    public function user_get_error_if_he_missed_first_name(){
        $register_data = [
            'last_name' => 'Dastanov',
            'email' => 'aibek@example.com',
            'password' => 'password',
        ];

        $response = $this->json('post', 'api/auth/register', $register_data);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['first_name']);
    }

    /**
     * @test
     */
    public function user_get_error_if_he_missed_last_name(){
        $register_data = [
            'first_name' => 'Aibek',
            'email' => 'aibek@example.com',
            'password' => 'password',
        ];

        $response = $this->json('post', 'api/auth/register', $register_data);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['last_name']);
    }

    /**
     * @test
     */
    public function user_get_error_if_he_missed_email(){
        $register_data = [
            'first_name' => 'Aibek',
            'last_name' => 'Dastanov',
            'password' => 'password',
        ];

        $response = $this->json('post', 'api/auth/register', $register_data);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['email']);
    }

    /**
     * @test
     */
    public function user_get_error_if_he_missed_password(){
        $register_data = [
            'first_name' => 'Aibek',
            'last_name' => 'Dastanov',
            'email' => 'aibek@example.com',
        ];

        $response = $this->json('post', 'api/auth/register', $register_data);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['password']);
    }
}
