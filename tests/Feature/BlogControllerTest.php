<?php

namespace Tests\Feature;

use App\Model\Category;
use App\Models\Blog;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BlogControllerTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * @test
     */
    public function getAllActiveNews()
    {
        $newsA = factory(Blog::class)->state('active')->create();
        $newsB = factory(Blog::class)->state('active')->create();
        $newsC = factory(Blog::class)->state('active')->create();
        factory(Blog::class)->state('notActive')->create();

        $response = $this->json('get', '/api/news');

        $response->assertStatus(200);
        $toArrayResponse = $response->original->items();

        $this->assertEquals(3, count($toArrayResponse));
        $this->assertTrue($newsA->is($toArrayResponse[0]));
        $this->assertTrue($newsB->is($toArrayResponse[1]));
        $this->assertTrue($newsC->is($toArrayResponse[2]));
    }

    /**
     * @test
     */
    public function getNewsByCategorySlug() {
        $categoryA = factory(Category::class)->create([
            'model' => Blog::class
        ]);
        $categoryB = factory(Category::class)->create([
            'model' => Blog::class
        ]);

        $newsA = factory(Blog::class)->state('active')->create([
            'category_id' => $categoryA->id
        ]);
        $newsB = factory(Blog::class)->state('active')->create([
            'category_id' => $categoryA->id
        ]);
        $newsC = factory(Blog::class)->state('active')->create([
            'category_id' => $categoryA->id
        ]);
        $newsD = factory(Blog::class)->state('active')->create([
            'category_id' => $categoryB->id
        ]);

        $response = $this->json('get', '/api/categories/news/'.$categoryA->slug);
        $response->assertStatus(200);
        $response->assertJsonCount(3);

        $this->assertTrue($response->original->contains($newsA));
        $this->assertTrue($response->original->contains($newsB));
        $this->assertTrue($response->original->contains($newsC));
        $this->assertFalse($response->original->contains($newsD));
    }

    /**
     * @test
     */
    public function user_can_get_news_by_slug() {
        $newsA = factory(Blog::class)->state('active')->create();

        $response = $this->json('get', '/api/news/'.$newsA->slug);

        $response->assertStatus(200);

        $response->assertJson($newsA->toArray());
    }

    /**
     * @test
     */
    public function user_get_error_if_blog_not_found() {
        $response = $this->json('get', '/api/news/wrong-slug');

        $response->assertStatus(404);

    }
}
