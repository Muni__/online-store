<?php

namespace Tests\Feature;

use App\Models\Banner;
use App\Models\Page;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Storage;
use PagesTableSeeder;
use Tests\TestCase;

class PageControllerTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * @test
     */
    public function get_page_about_company()
    {
        $this->seed();

        $response = $this->json('get', '/api/pages/about-company');
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'title',
            'key',
            'short_description',
            'description',
            'images'
        ]);
    }

    /**
     * @test
     */
    public function if_page_not_exist_show_error() {
        $response = $this->json('get', '/api/pages/wrong-key');
        $response->assertStatus(404);
    }
}
