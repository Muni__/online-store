<?php

namespace Tests\Feature;

use App\Model\Category;
use App\Model\Product;
use App\Models\Blog;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CategoriesControllerTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function can_get_all_product_categories()
    {
        factory(Category::class, 5)->create([
            'model' => Product::class
        ]);

        factory(Category::class, 5)->create([
            'model' => Blog::class
        ]);

        $response = $this->json('get', '/api/categories/product');

        $response->assertStatus(200);
        $response->assertJsonCount(5);
        $response->assertJsonStructure([
            '*' => [
                'name',
                'slug',
                'category_id',
                'model'
            ]
        ]);
    }

    /**
     * @test
     */
    public function can_get_all_blog_categories()
    {
        factory(Category::class, 5)->create([
            'model' => Blog::class
        ]);

        factory(Category::class, 5)->create([
            'model' => Product::class
        ]);

        $response = $this->json('get', '/api/categories/news');

        $response->assertStatus(200);
        $response->assertJsonCount(5);
        $response->assertJsonStructure([
            '*' => [
                'name',
                'slug',
                'category_id',
                'model'
            ]
        ]);
    }
}
