<?php

namespace Tests\Feature;

use App\Models\Contact;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ContactControllerTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * @test
     */
    public function get_contacts_of_company()
    {
        $this->withoutExceptionHandling();
        $email = Contact::create([
            'type' => 'email',
            'value' => 'company@example.com'
        ]);

        $phone = Contact::create([
            'type' => 'phone',
            'value' => '+996 999 99 99 99'
        ]);

        $address = Contact::create([
            'type' => 'address',
            'value' => 'Bishkek, str. Chui, 35'
        ]);

        $response = $this->json('get', '/api/contacts');
        $response->assertStatus(200);
        $response->assertJsonCount(3);

        $this->assertTrue($response->original->contains($email));
        $this->assertTrue($response->original->contains($phone));
        $this->assertTrue($response->original->contains($address));
    }
}
