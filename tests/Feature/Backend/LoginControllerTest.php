<?php

namespace Tests\Feature\Backend;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class LoginControllerTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * @test
     */
    public function admin_can_login_in_adminka()
    {
        $this->withoutExceptionHandling();
        factory(User::class)->create([
            'email' => 'admin@example.com',
            'password' => Hash::make('password'),
            'role_id' => Role::ADMIN
        ]);

        $loginData = [
            'email' => 'admin@example.com',
            'password' => 'password'
        ];

        $response = $this->json('post','api/admin/login', $loginData);

        $response->assertStatus(200);
        $response->assertJsonStructure(['access_token', 'token_type', 'expires_in']);
    }

    /**
     * @test
     */
    public function user_can_not_login_if_he_has_user_role() {
        factory(User::class)->create([
            'email' => 'aibek@example.com',
            'password' => Hash::make('password'),
            'role_id' => Role::USER
        ]);

        $loginData = [
            'email' => 'aibek@example.com',
            'password' => 'password'
        ];

        $response = $this->json('post','api/admin/login', $loginData);

        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function user_can_not_login_if_email_wrong(){
        factory(User::class)->create([
            'email' => 'aibek@example.com',
            'password' => Hash::make('password'),
            'role_id' => Role::ADMIN
        ]);

        $loginData = [
            'email' => 'wrong@example.com',
            'password' => 'password'
        ];

        $response = $this->json('post','api/admin/login', $loginData);

        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function user_can_not_login_if_password_wrong(){
        factory(User::class)->create([
            'email' => 'aibek@example.com',
            'password' => Hash::make('password'),
            'role_id' => Role::ADMIN
        ]);

        $loginData = [
            'email' => 'aibek@example.com',
            'password' => 'wrong_password'
        ];

        $response = $this->json('post','api/admin/login', $loginData);

        $response->assertStatus(401);
    }

    /**
     * @test
     */
    public function user_can_not_login_if_email_are_missed(){
        factory(User::class)->create([
            'email' => 'aibek@example.com',
            'password' => Hash::make('password'),
            'role_id' => Role::ADMIN
        ]);

        $loginData = [
            'password' => 'password'
        ];

        $response = $this->json('post','api/admin/login', $loginData);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['email']);
    }

    /**
     * @test
     */
    public function user_can_not_login_if_password_are_missed()
    {
        factory(User::class)->create([
            'email' => 'aibek@example.com',
            'password' => Hash::make('password'),
            'role_id' => Role::ADMIN
        ]);

        $loginData = [
            'email' => 'aibek@example.com',
        ];

        $response = $this->json('post', 'api/admin/login', $loginData);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['password']);
    }

    /**
     * @test
     */
    public function user_can_logout() {
        $user = factory(User::class)->create(['role_id' => Role::ADMIN]);

        $token = auth('api_admin')->login($user);

        $response = $this->json('post','api/admin/logout', [], ['Authorization' => 'Bearer '.$token]);
        $response->assertStatus(200);
        $response->assertJson([
            'message' => 'Successfully logged out',
        ]);
        $this->assertNull(Auth('api_admin')->user());
    }
}
