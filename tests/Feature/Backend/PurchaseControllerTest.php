<?php

namespace Tests\Feature\Backend;

use App\Model\Order;
use App\Model\Product;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PurchaseControllerTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * @test
     */
    public function admin_can_get_all_purchases()
    {
        $admin = factory(User::class)->create([
            'role_id' => Role::ADMIN
        ]);
        auth('api_admin')->login($admin);
        $userA = factory(User::class)->create();
        $userB = factory(User::class)->create();
        $orderA = factory(Order::class, 3)->create([
            'user_id' => $userA->id
        ]);
        $orderB = factory(Order::class, 2)->create([
            'user_id' => $userB->id
        ]);

        $order_items = [
            [
                'product_id' => 1,
                'sold_price' => 1500,
                'count' => 3,
            ],
            [
                'product_id' => 2,
                'sold_price' => 1500,
                'count' => 3,
            ],
            [
                'product_id' => 3,
                'sold_price' => 1500,
                'count' => 3,
            ]
        ];

        foreach ($orderA as $order) {
            $order->addItems($order_items);
        }
        foreach ($orderB as $order) {
            $order->addItems($order_items);
        }

        $response = $this->json('get', '/api/admin/purchases');
        $response->assertStatus(200);
        $response->assertJsonCount(5);
    }

    /**
     * @test
     */
    public function user_do_not_have_access_if_he_is_not_admin() {
        $user = factory(User::class)->create([
            'role_id' => Role::USER
        ]);
        $this->actingAs($user);
        factory(Order::class, 3)->create();

        $response = $this->json('get', '/api/admin/purchases');
        $response->assertStatus(403);
    }

    /**
     * @test
     */
    public function admin_can_get_order_by_id() {
        $admin = factory(User::class)->create([
            'role_id' => Role::ADMIN
        ]);
        auth('api_admin')->login($admin);
        $order = factory(Order::class)->create();

        $order_items = [
            [
                'product_id' => 1,
                'sold_price' => 1500,
                'count' => 3,
            ],
            [
                'product_id' => 2,
                'sold_price' => 1500,
                'count' => 3,
            ],
            [
                'product_id' => 3,
                'sold_price' => 1500,
                'count' => 3,
            ]
        ];

        $order->addItems($order_items);

        $response = $this->json('get', '/api/admin/purchases/'.$order->id);
        $response->assertStatus(200);

        $this->assertTrue($order->is($response->original));
    }

    /**
     * @test
     */
    public function admin_can_update_order() {
        $admin = factory(User::class)->create([
            'role_id' => Role::ADMIN
        ]);
        auth('api_admin')->login($admin);
        $order = factory(Order::class)->create();

        $order_items = [
            [
                'product_id' => 1,
                'sold_price' => 1500,
                'count' => 3,
            ],
            [
                'product_id' => 2,
                'sold_price' => 1500,
                'count' => 3,
            ],
            [
                'product_id' => 3,
                'sold_price' => 1500,
                'count' => 3,
            ]
        ];

        $order->addItems($order_items);

        $response = $this->json('put', '/api/admin/purchases/'.$order->id, [
            'delivery_address' => 'Street 123',
            'amount' => 5*1500,
            'status' => 'payed',
            'items' => $order_items
        ]);

        $response->assertStatus(201);
        $response->assertJsonStructure([
            'id',
            'amount',
            'delivery_address',
            'payment_status',
            'order_items' => [
                '*' => ['product_id', 'sold_price', 'count']
            ]
        ]);

        $order = Order::where('id', $order->id)->with('order_items')->first();

        $this->assertEquals(5*1500, $order->amount);
        $this->assertEquals('payed', $order->payment_status);
        $this->assertEquals(3, $order->order_items->count());
    }

    /**
     * @test
     */
    public function admin_can_delete_order() {
        $admin = factory(User::class)->create([
            'role_id' => Role::ADMIN
        ]);
        auth('api_admin')->login($admin);
        $order = factory(Order::class)->create();

        $order_items = [
            [
                'product_id' => 1,
                'sold_price' => 1500,
                'count' => 3,
            ],
            [
                'product_id' => 2,
                'sold_price' => 1500,
                'count' => 3,
            ],
            [
                'product_id' => 3,
                'sold_price' => 1500,
                'count' => 3,
            ]
        ];

        $order->addItems($order_items);

        $response = $this->json('delete', '/api/admin/purchases/'.$order->id);
        $response->assertStatus(204);
        $deletedOrder = Order::find($order->id);
        $this->assertNull($deletedOrder);

    }
}
