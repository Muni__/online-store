<?php

namespace Tests\Feature;

use App\Models\Banner;
use App\Models\Role;
use App\Models\User;
use Carbon\Traits\Mixin;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\Feature\Traits\AdminLoginTrait;
use Tests\TestCase;

class BannerControllerTest extends TestCase
{
    use DatabaseMigrations, AdminLoginTrait;

    /**
     * @test
     */
    public function get_all_active_banner_items()
    {
        $bannerA = factory(Banner::class)->state('active')->create([
           'position' => 1,
        ]);
        $bannerB = factory(Banner::class)->state('active')->create([
            'position' => 2,
        ]);
        $bannerC = factory(Banner::class)->state('active')->create([
            'position' => 3,
        ]);
        $bannerD = factory(Banner::class)->state('notActive')->create([
            'position' => 3,
        ]);

        $response = $this->json('get', '/api/banners');
        $response->assertStatus(200);
        $response->assertJsonCount(3);
        $response->assertJsonStructure([
            '*' => [
                'title',
                'description',
                'position',
                'status',
                'images'
            ]
        ]);

        $this->assertTrue($response->original->contains($bannerA));
        $this->assertTrue($response->original->contains($bannerB));
        $this->assertTrue($response->original->contains($bannerC));
        $this->assertFalse($response->original->contains($bannerD));
    }

    /**
     * @test
     */
    public function admin_set_active_status() {
        $this->auth_admin();

        $banner = factory(Banner::class)->state('notActive')->create();

        $response = $this->json('post', '/api/admin/banners/status/'.$banner->id, ['status' => 'active']);
        $banner->refresh();

        $response->assertStatus(201);
        $this->assertEquals('active', $banner->status);
    }

    /**
     * @test
     */
    public function get_all_banner_items()
    {
        $this->auth_admin();

        $bannerA = factory(Banner::class)->state('active')->create([
            'position' => 1,
        ]);
        $bannerB = factory(Banner::class)->state('active')->create([
            'position' => 2,
        ]);
        $bannerC = factory(Banner::class)->state('notActive')->create([
            'position' => 3,
        ]);

        $response = $this->json('get', '/api/admin/banners');
        $response->assertStatus(200);
        $toArrayResponse = $response->original->items();

        $this->assertEquals(3, count($toArrayResponse));
        $this->assertTrue($bannerA->is($toArrayResponse[0]));
        $this->assertTrue($bannerB->is($toArrayResponse[1]));
        $this->assertTrue($bannerC->is($toArrayResponse[2]));
    }

    /**
     * @test
     */
    public function admin_get_info_about_banner_by_id() {
        $this->auth_admin();

        $banner = factory(Banner::class)->create();

        $response = $this->json('get', '/api/admin/banners/'.$banner->id);

        $response->assertStatus(200);
        $response->assertJson($banner->toArray());
    }

    /**
     * @test
     */
    public function admin_can_create_banner()
    {
        $this->auth_admin();

        $_banner = [
            'title' => 'Banner title',
            'description' => 'Banner description',
            'position' => 1,
        ];

        $response = $this->json('post', '/api/admin/banners', $_banner);

        $banner = Banner::first();

        $response->assertStatus(201);
        $this->assertEquals('Banner title', $banner->title);
        $this->assertEquals('Banner description', $banner->description);
        $this->assertEquals(1, $banner->position);
    }

    /**
     * @test
     */
    public function admin_can_update_banner() {
        $this->auth_admin();

        $banner = factory(Banner::class)->create();

        $_banner = [
            'title' => 'Banner title updated',
            'description' => 'Banner description updated',
            'position' => 1
        ];

        $response = $this->json('put', '/api/admin/banners/'.$banner->id, $_banner);
        $response->assertStatus(201);
        $banner->refresh();
        $this->assertEquals($banner->title, 'Banner title updated');
        $this->assertEquals($banner->description, 'Banner description updated');
        $this->assertEquals($banner->position, 1);
    }

    /**
     * @test
     */
    public function admin_can_delete_banner() {
        $this->auth_admin();

        $banner = factory(Banner::class)->create();

        $response = $this->json('delete', '/api/admin/banners/'.$banner->id);
        $response->assertStatus(204);
        $deleted = Banner::find($banner->id);
        $this->assertNull($deleted);
    }

    /**
     * @test
     */
    public function admin_can_upload_banner_photos() {
        $this->auth_admin();

        Storage::fake('photos');

        $banner = factory(Banner::class)->create();

        $response = $this->json('POST', '/api/admin/banners/upload/'.$banner->id, [
            UploadedFile::fake()->image('photo1.jpg'),
            UploadedFile::fake()->image('photo2.jpg')
        ]);

        $response->assertStatus(201);
        $banner->load('images');
        $this->assertEquals(2, $banner->images->count());
        $this->assertEquals('photo1.jpg', $banner->images[0]->client_file_name);
        $this->assertEquals('photo2.jpg', $banner->images[1]->client_file_name);
    }
}
