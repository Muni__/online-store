<?php

namespace Tests\Feature\Traits;

use App\Models\Role;
use App\Models\User;

trait AdminLoginTrait {
    public function auth_admin() {
        $admin = factory(User::class)->create([
            'role_id' => Role::ADMIN
        ]);
        auth('api_admin')->login($admin);
    }
}
