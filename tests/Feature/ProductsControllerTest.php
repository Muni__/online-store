<?php

namespace Tests\Feature;

use App\Model\Discount;
use App\Model\Product;
use App\Model\Category;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\Feature\Traits\AdminLoginTrait;
use Tests\TestCase;

class ProductsControllerTest extends TestCase
{
    use DatabaseMigrations, AdminLoginTrait;

    /**
     * @test
     */
    public function can_get_all_active_products()
    {
        $productA = factory(Product::class)->state('active')->create();
        $productB = factory(Product::class)->state('active')->create();
        $productC = factory(Product::class)->state('active')->create();
        factory(Product::class)->state('notActive')->create();

        $response = $this->json('get', '/api/products');

        $response->assertStatus(200);
        $toArrayResponse = $response->original->items();

        $this->assertEquals(3, count($toArrayResponse));
        $this->assertTrue($productA->is($toArrayResponse[0]));
        $this->assertTrue($productB->is($toArrayResponse[1]));
        $this->assertTrue($productC->is($toArrayResponse[2]));
    }

    /**
     * @test
     */
    public function can_get_active_product_info() {
        $product = factory(Product::class)->state('active')->create();

        $response = $this->json('get', '/api/products/'.$product->id);

        $response->assertStatus(200);

        $response->assertJson($product->toArray());
    }

    /**
     * @test
     */
    public function user_cannot_get_nonActive_product() {
        $product = factory(Product::class)->state('notActive')->create();

        $response = $this->json('get', '/api/products/'.$product->id);

        $response->assertStatus(404);
    }

    /**
     * @test
     */
    public function user_get_error_if_product_not_found() {
        $response = $this->json('get', '/api/products/99');

        $response->assertStatus(404);
    }

    /**
     * @test
     */
    public function can_get_products_by_category_id() {
        $categoryA = factory(Category::class)->create();
        $categoryB = factory(Category::class)->create();
        $productA = factory(Product::class)->state('active')->create([
            'category_id' => $categoryA->id
        ]);
        $productB = factory(Product::class)->state('active')->create([
            'category_id' => $categoryA->id
        ]);
        $productC = factory(Product::class)->state('active')->create([
            'category_id' => $categoryA->id
        ]);
        $productD = factory(Product::class)->state('active')->create([
            'category_id' => $categoryB->id
        ]);

        $response = $this->json('get', '/api/categories/product/'.$categoryA->slug);

        $response->assertStatus(200);

        $response->assertJsonCount(3);
        $this->assertTrue($response->original->contains($productA));
        $this->assertTrue($response->original->contains($productB));
        $this->assertTrue($response->original->contains($productC));
        $this->assertFalse($response->original->contains($productD));
    }

    /**
     * @test
     */
    public function admin_set_active_status() {
        $this->auth_admin();
        $product = factory(Product::class)->state('notActive')->create();

        $response = $this->json('post', '/api/admin/products/status/'.$product->id, ['status' => 'active']);
        $product->refresh();

        $response->assertStatus(201);
        $this->assertEquals('active', $product->status);
    }

    /**
     * @test
     */
    public function admin_can_create_product() {
        $this->auth_admin();
        $category = factory(Category::class)->create();
        $discount = factory(Discount::class)->create();
        $_product = [
            'title' => 'Product title',
            'short_description' => 'Short description of product',
            'description' =>  'Description of product',
            'category_id' => $category->id,
            'discount_id' => $discount->id,
            'price' => 2000,
            'status' => 'active'
        ];

        $response = $this->json('post', '/api/admin/products', $_product);
        $response->assertStatus(201);
        $product = Product::first();
        $this->assertEquals($product->title, "Product title");
        $this->assertEquals($product->short_description, "Short description of product");
        $this->assertEquals($product->description, "Description of product");
        $this->assertEquals($product->category_id, $category->id);
        $this->assertEquals($product->discount_id, $discount->id);
        $this->assertEquals($product->price, 2000);
        $this->assertEquals($product->status, 'active');
    }

    /**
     * @test
     */
    public function admin_can_update_product() {
        $this->auth_admin();
        $product = factory(Product::class)->state('active')->create();

        $_product = [
            'title' => 'Product title updated',
            'short_description' => 'Short description of product updated',
            'description' =>  'Description of product updated',
            'category_id' => $product->category_id,
            'price' => 2000,
            'status' => 'active'
        ];

        $response = $this->json('put', '/api/admin/products/'.$product->id, $_product);
        $response->assertStatus(201);
        $product->refresh();
        $this->assertEquals($product->title, "Product title updated");
        $this->assertEquals($product->short_description, "Short description of product updated");
        $this->assertEquals($product->description, "Description of product updated");
        $this->assertEquals($product->category_id, $_product['category_id']);
        $this->assertEquals($product->price, 2000);
        $this->assertEquals($product->status, 'active');
    }

    /**
     * @test
     */
    public function admin_can_delete_product() {
        $this->auth_admin();
        $product = factory(Product::class)->state('active')->create();

        $response = $this->json('delete', '/api/admin/products/'.$product->id);
        $response->assertStatus(204);
        $deletedProduct = Product::find($product->id);
        $this->assertNull($deletedProduct);
    }

    /**
     * @test
     */
    public function admin_can_upload_product_photos() {
        Storage::fake('photos');
        $this->auth_admin();
        $product = factory(Product::class)->state('active')->create();

        $response = $this->json('POST', '/api/admin/products/upload/'.$product->id, [
            UploadedFile::fake()->image('photo1.jpg'),
            UploadedFile::fake()->image('photo2.jpg')
        ]);

        $response->assertStatus(201);
        $product->load('images');
        $this->assertEquals(2, $product->images->count());
        $this->assertEquals('photo1.jpg', $product->images[0]->client_file_name);
        $this->assertEquals('photo2.jpg', $product->images[1]->client_file_name);
    }
}
