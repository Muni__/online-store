<?php

namespace App\Model;

use App\Models\Blog;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use Sluggable;
    protected $guarded = [];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function products() {
        return $this->hasMany(Product::class, 'category_id');
    }

    public function news() {
        return $this->hasMany(Blog::class, 'category_id');
    }
}
