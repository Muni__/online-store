<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $guarded = [];

    public function images() {
        return $this->morphTomany(Media::class, 'imageable');
    }
}
