<?php

namespace App\Models;

use App\Services\MediaService\Facades\MediaService;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $guarded = [];

    public function setConversionsAttribute($value)
    {
        $this->attributes['conversions'] = json_encode($value);
    }

    public function getNameBySize($size) {
        return $this->conversions[$size]['name'];
    }

    public function getConversionsAttribute($value)
    {
        $conversions = json_decode($value, true);

        foreach ($conversions as $k => $conversion)
        {
            $conversions[$k]['url'] = asset(config('media.path') . $conversion['name']);
        }

        return $conversions;

    }

    public function getOriginalFileNameAttribute($value)
    {
        return asset(config('media.path').$value);
    }

    /**
     * Установка изображения главным
     */
    public function setMainImage(): void
    {
        self::where('imageable_type', $this->imageableType)
            ->where('imageable_id', $this->imageableId)
            ->update(['main_image' => 0]);

        $this->main_image = 1;
        $this->save();
    }

    protected static function boot() {
        parent::boot();
        static::deleting(function($media) {
            MediaService::delete($media);
        });
    }
}
