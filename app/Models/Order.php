<?php

namespace App\Model;

use App\Models\Promocode;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use PhpParser\Builder;
use Request;

class Order extends Model
{
    protected $guarded = [];

    public function order_items() {
        return $this->hasMany(OrderItem::class);
    }

    public static function saveOrder(array $orderData, array $orderItemsData) {

        DB::beginTransaction();
        try {
            $order = self::create($orderData);
            $order->addItems($orderItemsData);
            if ($order->promocode) {
                $order->promocode->used();
            }

            DB::commit();

            $order->load('order_items');
            return $order;
        }catch (\Exception $exception) {
            DB::rollBack();
            throw new ModelNotFoundException('Order has not created');
        }
    }

    /**
     * @param array $order_items
     * @return Order
     */
    public function addItems(array $order_items) {
        foreach ($this->order_items as $item) {
            $item->delete();
        }
        foreach ($order_items as $orderItem) {
            $this->order_items()->create($orderItem);
        }
        return $this;
    }

    /**
     * @param $query
     * @param array $filter
     * @return
     */
    public function scopeFilter($query, array $filter) {
        if (isset($filter['status'])) {
            $query->where('payment_status', $filter['status']);
        }

        if (isset($filter['email'])) {
            $email = $filter['email'];
            $query->whereHas('user', function ($query) use ($email) {
                return $query->where('email', $email);
            });
        }

        return $query;
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function promocode() {
        return $this->belongsTo(Promocode::class)->with('discount');
    }

}
