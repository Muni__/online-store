<?php

namespace App\Models;

use App\Models\Traits\ChangeStatusTrait;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    use ChangeStatusTrait;

    protected $guarded = [];

    public function images() {
        return $this->morphTomany(\App\Models\Media::class, 'imageable');
    }
}
