<?php

namespace App\Models;

use App\Model\Category;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use Sluggable;
    protected $guarded = [];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function images() {
        return $this->morphTomany(\App\Models\Media::class, 'imageable');
    }

    public function category() {
        return $this->belongsTo(Category::class);
    }
}
