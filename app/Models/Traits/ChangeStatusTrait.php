<?php

namespace App\Models\Traits;

trait ChangeStatusTrait {
    /**
     * @param string $status
     */
    public function changeStatus(string $status) {
        $this->status = $status;
        $this->save();
    }
}
