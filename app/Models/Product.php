<?php

namespace App\Model;

use App\Models\Traits\ChangeStatusTrait;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use ChangeStatusTrait;

    protected $guarded = [];

    public function getFormattedPriceAttribute()
    {
        return number_format($this->price/100, 2);
    }

    public function scopeActive($query) {
        return $query->where('status', 'active');
    }

    public function category() {
        return $this->belongsTo(Category::class);
    }

    public function discount()
    {
        return $this->belongsTo(Discount::class);
    }

    public function images() {
        return $this->morphToMany(\App\Models\Media::class, 'imageable');
    }

    protected static function boot() {
        parent::boot();
        static::deleting(function($product) {
            foreach ($product->images()->get() as $image) {
                $image->delete();
            }
        });
    }

}
