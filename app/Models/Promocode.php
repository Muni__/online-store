<?php

namespace App\Models;

use App\Model\Discount;
use Illuminate\Database\Eloquent\Model;

class Promocode extends Model
{
    protected $guarded = [];

    public function used() {
        $this->number_of_uses++;
        $this->save();
    }

    public function Discount()
    {
        return $this->belongsTo(Discount::class);
    }
}
