<?php

namespace App\Mail;

use App\Models\ConfirmCode;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Str;

class RemindPassword extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var User $user
     */
    public $user;

    /**
     * @var ConfirmCode $confirmCode
     */
    public $confirmCode;

    /**
     * @var String $url
     */
    public $url;

    /**
     * Create a new message instance.
     *
     * @param User $user
     * @param ConfirmCode $confirmCode
     * @param String $url
     */
    public function __construct(User $user, ConfirmCode $confirmCode, String $url)
    {
        $this->user = $user;
        $this->confirmCode = $confirmCode;
        $this->url = $url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.remindPassword');
    }
}
