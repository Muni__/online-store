<?php

namespace App\Http\Controllers;

use App\Http\Requests\BannerRequest;
use App\Models\Banner;
use App\Services\MediaService\Facades\MediaService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    /**
     * @var Banner $banner
     */
    protected $banner;

    /**
     * BannerController constructor.
     * @param Banner $banner
     */
    public function __construct(Banner $banner)
    {
        $this->banner = $banner;
    }

    /**
     * @return JsonResponse
     */
    public function active_list() {
        $banners = $this->banner->where('status', 'active')->orderBy('position', 'asc')->with('images')->get();

        return response()->json($banners, 200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function list(Request $request) {
        $paginate_count = $request->has('items') ? $request->input('items') : 10;

        $banner = $this->banner->orderBy('position', 'asc')->with('images')->paginate($paginate_count);

        return response()->json($banner, 200);
    }

    /**
     * @param int $banner_id
     * @return JsonResponse
     */
    public function item(int $banner_id){
        $banner = $this->banner->findOrfail($banner_id);

        return response()->json($banner, 200);
    }

    /**
     * @param Request $request
     * @param int $banner_id
     * @return JsonResponse
     */
    public function changeStatus(Request $request, int $banner_id) {
        if ($request->has('status')) {
            $banner = $this->banner->findOrfail($banner_id);
            if ($request->input('status') == 'active' || $request->input('notActive')) {
                $banner->changeStatus($request->input('status'));
                return response()->json('Status has changed', 201);
            }

            return response()->json('Data are nor correct', 400);
        }

        return response()->json('Data are  empty', 400);
    }

    /**
     * @param BannerRequest $request
     * @return JsonResponse
     */
    public function store(BannerRequest $request) {
        $banner = $this->banner->create($request->all());

        if ($banner) {
            return response()->json($banner, 201);
        }

        return response()->json('Banner has not created', 400);
    }

    /**
     * @param BannerRequest $request
     * @param int $banner_id
     * @return JsonResponse
     */
    public function update(BannerRequest $request, int $banner_id) {
        $banner = $this->banner->findOrFail($banner_id)->update($request->all());
        if ($banner) {
            return response()->json($banner, 201);
        }

        return response()->json('Banner has not updated', 400);
    }

    /**
     * @param int $banner_id
     * @return JsonResponse
     */
    public function delete(int $banner_id) {
        $banner = $this->banner->findOrFail($banner_id);

        if($banner->delete()){
            return response()->json('Banner was deleted', 204);
        }

        return response()->json([
            'error' => 'Banner has not deleted'
        ], 404);
    }

    /**
     * @param Request $request
     * @param int $banner_id
     * @return JsonResponse
     */
    public function upload(Request $request, int $banner_id) {
        $banner = $this->banner->findOrFail($banner_id);

        $allFiles = $request->allFiles();

        if(!count($allFiles)) {
            return response()->json('You have not submitted file(-s)', 400);
        }

        try {
            foreach ($allFiles as $file) {
                MediaService::upload($file, get_class($banner), $banner->id);
            }
        }catch (\Exception $exception) {
            return response()->json('Something went wrong, please try again.', 400);
        }

        return response()->json('File(-s) was uploaded', 201);
    }
}
