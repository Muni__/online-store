<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * @var Page $page
     */
    private $page;

    /**
     * PageController constructor.
     * @param Page $page
     */
    public function __construct(Page $page)
    {
        $this->page = $page;
    }

    public function getByKey(string $key) {
        $page = $this->page->where('key', $key)->with('images')->firstOrfail();

        return response()->json($page, 200);
    }
}
