<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RemindPasswordRequest;
use App\Http\Requests\RemindRequest;
use App\Mail\RemindPassword;
use App\Models\ConfirmCode;
use App\Models\Role;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->except('login', 'remind', 'confirmCode', 'changePassword');
    }

    /**
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function login(LoginRequest $request) {
        $credentials = $request->json()->all();


        if (! $token = auth('api')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * @return JsonResponse
     */
    public function logout() {
        auth('api')->logout();

        return response()->json(['message' => 'Successfully logged out'], 200);
    }

    /**
     * @param RemindRequest $request
     * @return JsonResponse
     */
    public function remind(RemindRequest $request) {
        $user = User::where(['email' => $request->input('email'), 'role_id' => Role::USER])->firstOrFail();
        $confirmCode = ConfirmCode::create([
            'code' => Str::random(20),
            'expire' => Carbon::now()->addHour(),
            'user_id' => $user->id
        ]);
        Mail::to($user->email)->queue(new RemindPassword($user, $confirmCode, $request->input('url')));
        return response()->json('Email was sent. Please follow to instructions in the email.');
    }

    /**
     * @param string $code
     * @return JsonResponse
     */
    public function confirmCode(string $code) {
        $code = ConfirmCode::where('code', $code)->firstOrFail();

        if (Carbon::now()->gt(Carbon::create($code->expire))) {
            return response()->json('Time confirm code was expired', 404);
        }

        return response()->json(true, 200);
    }

    /**
     * @param string $code
     * @param RemindPasswordRequest $request
     * @return JsonResponse
     */
    public function changePassword(string $code, RemindPasswordRequest $request) {
        $code = ConfirmCode::where('code', $code)->with('user')->firstOrFail();
        if (Carbon::now()->gt(Carbon::create($code->expire))) {
            return response()->json('Time confirm code was expired', 404);
        }
        $code->user->password = \Hash::make($request->input('password'));
        $code->user->save();

        return response()->json('Password was changed.', 201);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
