<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Mail\SuccessfulRegistration;
use App\Models\User;
use Hash;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    /**
     * @var User $user
     */
    protected $user;

    /**
     * RegisterController constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function register(RegisterRequest $request) {
        $user = $this->createUser($request->all());
        if($user) {
            Mail::to($user->email)->queue(new SuccessfulRegistration($user));
            return response()->json('You are successfully registered', 201);
        }

        return response()->json('Something went wrong', 400);
    }

    private function createUser(array $data) {
        return User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
