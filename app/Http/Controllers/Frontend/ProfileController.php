<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\ProfileRequest;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    /**
     * @var User $user
     */
    protected $user;

    /**
     * ProfileController constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return JsonResponse
     */
    public function list(){
        $user = auth()->user();

        return response()->json($user, 200);
    }

    /**
     * @param ProfileRequest $request
     * @return JsonResponse
     */
    public function update(ProfileRequest $request) {
        $user = auth()->user();
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->phone = $request->input('phone');
        $user->save();
        $user->refresh();

        return response()->json($user, 201);
    }

    /**
     * @param ChangePasswordRequest $request
     * @return JsonResponse
     */
    public function changePassword(ChangePasswordRequest $request) {
        $user = auth()->user();
        if (Hash::check($request->input('oldPassword'), $user->password)) {
            $user->password = Hash::make($request->input('password'));
            $user->save();
        }

        return response()->json('Password successfully updated', 201);
    }
}
