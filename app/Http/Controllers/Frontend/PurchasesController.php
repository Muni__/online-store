<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrderRequest;
use App\Model\Order;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PurchasesController extends Controller
{
    /**
     * @var Order $order
     */
    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function list(Request $request) {
        $paginate_count = $request->has('items') ? $request->input('items') : 10;

        $orders = auth()->user()->orders()->with(['order_items', 'Promocode'])->paginate($paginate_count);

        return response()->json($orders, 200);
    }

    /**
     * @param OrderRequest $request
     * @return JsonResponse
     */
    public function store(OrderRequest $request) {
        $user = auth()->user();
        $request->merge(['user_id' => $user->id]);

        try {
            $order = Order::saveOrder($request->only(['amount', 'delivery_address', 'user_id', 'promocode_id']), $request->json('items'));
        }catch (\Exception $exception) {
            return response()->json($exception->getMessage(), 400);
        }

        return response()->json($order->load('order_items'), 201);
    }
}
