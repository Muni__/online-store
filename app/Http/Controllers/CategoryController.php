<?php

namespace App\Http\Controllers;

use App\Model\Category;
use App\Model\Product;
use App\Models\Blog;
use Illuminate\Http\JsonResponse;

class CategoryController extends Controller
{
    /**
     * @var Category $category;
     */
    private $category;

    /**
     * CategoryController constructor.
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    /**
     * @return JsonResponse
     */
    public function productCategories() {
        $categories = $this->category->where('model', Product::class)->get();

        return response()->json($categories, 200);
    }

    /**
     * @return JsonResponse
     */
    public function blogCategories() {
        $categories = $this->category->where('model', Blog::class)->get();

        return response()->json($categories, 200);
    }
}
