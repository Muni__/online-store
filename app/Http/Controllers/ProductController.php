<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Model\Product;
use App\Model\Category;
use App\Services\MediaService\Facades\MediaService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * @var Product $product
     */
    protected $product;

    /**
     * @var Category $productCategory
     */
    protected $productCategory;

    /**
     * ProductController constructor.
     * @param Product $product
     * @param Category $productCategory
     */
    public function __construct(Product $product, Category $productCategory)
    {
        $this->product = $product;
        $this->productCategory = $productCategory;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function list(Request $request) {
        $paginate_count = $request->has('items') ? $request->input('items') : 10;

        $products =  $this->product->with(['category', 'discount', 'images'])->active()->paginate($paginate_count);

        return response()->json($products, 200);
    }

    /**
     * @param int $product_id
     * @return JsonResponse
     */
    public function item(int $product_id) {
        $product =  $this->product->with('category', 'discount', 'images')->active()->findOrFail($product_id);

        return response()->json($product, 200);
    }

    /**
     * @param string $category_slug
     * @return JsonResponse
     */
    public function getByCategory(string $category_slug) {
        $category = $this->productCategory->where(['slug' => $category_slug, 'model' => Product::class])->with('products')->first();
        abort_if(!$category, 404);

        return response()->json($category->products, 200);
    }

    /**
     * @param ProductRequest $request
     * @return JsonResponse
     */
    public function store(ProductRequest $request) {
        $product = $this->product->create($request->all());
        if ($product) {
            return response()->json($product, 201);
        }

        return response()->json('Product has not created', 400);
    }

    /**
     * @param Request $request
     * @param int $product_id
     * @return JsonResponse
     */
    public function changeStatus(Request $request, int $product_id) {
        if ($request->has('status')) {
            $product = $this->product->findOrfail($product_id);
            if ($request->input('status') == 'active' || $request->input('notActive')) {
                $product->changeStatus($request->input('status'));
                return response()->json('Status has changed', 201);
            }

            return response()->json('Data are nor correct', 400);
        }

        return response()->json('Data are  empty', 400);
    }

    /**
     * @param ProductRequest $request
     * @param int $product_id
     * @return JsonResponse
     */
    public function update(ProductRequest $request, int $product_id) {
        $product = $this->product->findOrFail($product_id)->update($request->all());
        if ($product) {
            return response()->json($product, 201);
        }

        return response()->json('Product has not updated', 400);
    }

    /**
     * @param int $product_id
     * @return JsonResponse
     */
    public function delete(int $product_id) {
        $product = $this->product->findOrFail($product_id);

        if($product->delete()){
            return response()->json('Product was deleted', 204 );
        }

        return response()->json([
            'error' => 'Product has not deleted'
        ], 404);
    }

    /**
     * @param Request $request
     * @param int $product_id
     * @return JsonResponse
     */
    public function upload(Request $request, int $product_id) {
        $product = $this->product->findOrFail($product_id);

        $allFiles = $request->allFiles();

        if(!count($allFiles)) {
            return response()->json('You have not submitted file(-s)', 400);
        }

        try {
            foreach ($allFiles as $file) {
                MediaService::upload($file, get_class($product), $product->id);
            }
        }catch (\Exception $exception) {
            return response()->json('Something went wrong, please try again.', 400);
        }

        return response()->json('File(-s) was uploaded', 201);
    }
}
