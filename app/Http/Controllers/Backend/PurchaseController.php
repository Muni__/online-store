<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrderRequest;
use App\Model\Order;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PurchaseController extends Controller
{
    /**
     * @var Order $order
     */
    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function list(Request $request) {
        $orders = $this->order->with(['order_items', 'Promocode'])
            ->orderBy($request->input('orderBy', 'created_at'), $request->input('order', 'desc'))
            ->filter($request->only(['status', 'email']))
            ->get();

        return response()->json($orders, 200);
    }

    /**
     * @param int $order_id
     * @return JsonResponse
     */
    public function item(int $order_id) {
        $order = $this->order->with(['order_items', 'Promocode'])->findOrFail($order_id);

        return response()->json($order, 200);
    }

    /**
     * @param OrderRequest $request
     * @param int $order_id
     * @return JsonResponse
     * @throws \Throwable
     */
    public function update(OrderRequest $request, int $order_id) {
        $order = $this->order->findOrFail($order_id);

        DB::beginTransaction();
        try {
            $order->update([
                'delivery_address' => $request->input('delivery_address'),
                'amount' =>  $request->input('amount'),
                'payment_status' =>  $request->input('status')
            ]);
            $order->addItems($request->input('items'));
            DB::commit();
            $order->load('order_items');
            return response()->json($order->load('order_items'), 201);
        }catch (\Exception $exception) {
            DB::rollBack();
            return response()->json('Order has not created', 400);
        }
    }

    /**
     * @param int $order_id
     * @return JsonResponse
     */
    public function delete(int $order_id) {
        $order = $this->order->findOrFail($order_id);

        if($order->delete()){
            return response()->json('Order was deleted', 204 );
        }

        return response()->json([
            'error' => 'Order has not deleted'
        ], 404);
    }
}
