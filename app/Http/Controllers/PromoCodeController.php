<?php

namespace App\Http\Controllers;

use App\Http\Requests\PromoCodeRequest;
use App\Models\Promocode;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PromoCodeController extends Controller
{
    /**
     * @var Promocode $promoCode
     */
    protected $promoCode;

    /**
     * PromoCodeController constructor.
     * @param Promocode $promoCode
     */
    public function __construct(Promocode $promoCode)
    {
        $this->promoCode = $promoCode;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function list(Request $request) {
        $paginate_count = $request->has('items') ? $request->input('items') : 10;

        $promoCodes = $this->promoCode->with('discount')->paginate($paginate_count);

        return response()->json($promoCodes, 200);
    }

    /**
     * @param int $promocode_id
     * @return JsonResponse
     */
    public function item(int $promocode_id){
        $promoCode = $this->promoCode->with('discount')->findOrfail($promocode_id);

        return response()->json($promoCode, 200);
    }

    /**
     * @param string $code
     * @return JsonResponse
     */
    public function check(string $code) {
        $promoCode = $this->promoCode->with('discount')->where('code', $code)->first();

        if (!$promoCode) {
            return response()->json('PromoCode not found', 400);
        }

        if ($promoCode->type == 'disposable' && $promoCode->number_of_uses != 0) {
            return response()->json('PromoCode was used', 400);
        }

        return response()->json($promoCode, 200);
    }

    /**
     * @param PromoCodeRequest $request
     * @return JsonResponse
     */
    public function store(PromoCodeRequest $request) {
        $promoCode = $this->promoCode->create($request->all());

        if ($promoCode) {
            return response()->json($promoCode, 201);
        }

        return response()->json('PromoCode has not created', 400);
    }

    /**
     * @param PromoCodeRequest $request
     * @param int $promocode_id
     * @return JsonResponse
     */
    public function update(PromoCodeRequest $request, int $promocode_id) {
        $promoCode = $this->promoCode->findOrFail($promocode_id)->update($request->all());
        if ($promoCode) {
            return response()->json($promoCode, 201);
        }

        return response()->json('PromoCode has not updated', 400);
    }

    /**
     * @param int $promocode_id
     * @return JsonResponse
     */
    public function delete(int $promocode_id) {
        $promoCode = $this->promoCode->findOrFail($promocode_id);

        if($promoCode->delete()){
            return response()->json('PromoCode was deleted', 204 );
        }

        return response()->json([
            'error' => 'PromoCode has not deleted'
        ], 404);
    }
}
