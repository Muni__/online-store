<?php

namespace App\Http\Controllers;

use App\Model\Category;
use App\Models\Blog;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
;

class BlogController extends Controller
{
    /**
     * @var Blog $blog
     */
    private $blog;

    /**
     * @var Category $category
     */
    private $category;

    /**
     * BlogController constructor.
     * @param Blog $blog
     * @param Category $category
     */
    public function __construct(Blog $blog, Category $category)
    {
        $this->blog = $blog;
        $this->category = $category;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function list(Request $request) {
        $paginate_count = $request->has('items') ? $request->input('items') : 10;

        $news = $this->blog->where('status', 'active')->with(['images', 'category'])->paginate($paginate_count);

        return response()->json($news, 200);
    }

    /**
     * @param string $category_slug
     * @return JsonResponse
     */
    public function getByCategory(string $category_slug) {
        $category = $this->category->where(['slug' => $category_slug, 'model' => Blog::class])->with('news')->first();
        abort_if(!$category, 404);

        return response()->json($category->news, 200);
    }

    /**
     * @param string $slug
     * @return JsonResponse
     */
    public function item(string $slug) {
        $news = $this->blog->where('slug', $slug)->with(['images', 'category'])->firstOrFail();

        return response()->json($news, 200);
    }
}
