<?php

namespace App\Http\Controllers;

use App\Http\Requests\DiscountRequest;
use App\Model\Discount;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class DiscountController extends Controller
{
    /**
     * @var Discount $discount
     */
    protected $discount;

    /**
     * DiscountController constructor.
     * @param Discount $discount
     */
    public function __construct(Discount $discount)
    {
        $this->discount = $discount;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function list(Request $request) {
        $paginate_count = $request->has('items') ? $request->input('items') : 10;

        $discount = $this->discount->paginate($paginate_count);

        return response()->json($discount, 200);
    }

    /**
     * @param int $discount_id
     * @return JsonResponse
     */
    public function item(int $discount_id){
        $discount = $this->discount->findOrfail($discount_id);

        return response()->json($discount, 200);
    }

    /**
     * @param DiscountRequest $request
     * @return JsonResponse
     */
    public function store(DiscountRequest $request) {
        $discount = $this->discount->create($request->all());

        if ($discount) {
            return response()->json($discount, 201);
        }

        return response()->json('Discount has not created', 400);
    }

    /**
     * @param DiscountRequest $request
     * @param int $discount_id
     * @return JsonResponse
     */
    public function update(DiscountRequest $request, int $discount_id) {
        $promoCode = $this->discount->findOrFail($discount_id)->update($request->all());
        if ($promoCode) {
            return response()->json($promoCode, 201);
        }

        return response()->json('Discount has not updated', 400);
    }

    /**
     * @param int $discount_id
     * @return JsonResponse
     */
    public function delete(int $discount_id) {
        $promoCode = $this->discount->findOrFail($discount_id);

        if($promoCode->delete()){
            return response()->json('Discount was deleted', 204 );
        }

        return response()->json([
            'error' => 'Discount has not deleted'
        ], 404);
    }

}
