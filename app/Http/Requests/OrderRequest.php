<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (bool)auth()->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => 'required',
            'delivery_address' => 'required',
            'items' => 'required|array',
            'items.*' => 'array',
            'items.*.product_id' => 'required',
            'items.*.sold_price' => 'required',
            'items.*.count' => 'required'
        ];
    }

    public function all($keys = null){
        if(empty($keys)){
            return parent::json()->all();
        }

        return collect(parent::json()->all())->only($keys)->toArray();
    }

    public function only($keys)
    {
        return collect(parent::json()->all())->only($keys)->toArray();
    }
}
