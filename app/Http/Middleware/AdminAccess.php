<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AdminAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth('api_admin')->user();
        if ($user !== null) {
            if ($user->isAdmin()) {
                return $next($request);
            }
        }

        abort(403, 'You do not have access');
    }
}
