<?php


namespace App\Services\MediaService;

use Exception;

// facades
use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image;

// models
use App\Models\Media;

class MediaService
{
    /**
     * Загрузка изображения
     * @param UploadedFile $image
     * @param string $imageableType
     * @param int $imageableId
     * @param array $conversions
     * @return mixed
     * @throws Exception
     */
    public function upload(UploadedFile $image, string $imageableType, int $imageableId, array $conversions = []): Media
    {
        $this->check($image, $conversions);

        $clientName = $image->getClientOriginalName();

        $originalFileName = uniqid() . '.' . $image->guessClientExtension();

        // создаем инстанс Intervention Image
        $img = Image::make($image);

        $converted = $this->saveInStore($img, $conversions, $originalFileName);

        $size = $img->filesize();
        $mime = $img->mime();

        $imageableTypeModel = $imageableType::where('id', $imageableId)
            ->with('images')
            ->first();

        $mediaData = [
            'client_file_name' => $clientName,
            'original_file_name' => $originalFileName,
            'path' => asset(config('media.path')),
            'conversions' => $converted,
            'size' => $size,
            'mime' => $mime
        ];

        if (!$imageableTypeModel->count()) {
            $mediaData['main_image'] = 1;
        }

        $media = Media::create($mediaData);

        $imageableTypeModel
                ->images()
                ->save($media, [
                    'imageable_type' => $imageableType,
                    'imageable_id' => $imageableId
                ]);
        return $media;
    }

    /**
     * Приватный метод для удаления изображений для модели
     * @param Media $media
     * @throws Exception
     */
    public function delete(Media $media): void
    {
        /**
         * Если удаляемое изображение является главным для сущности
         * то найдем первое попавшееся не главное и сделаем его главным
         */
        if ($media->main_image) {
            $mainMedia = Media::where('imageable_type', $media->imageable_type)
                ->where('imageable_id', $media->imageable_id)
                ->where('id', '!=', $media->id)
                ->where('main_image', 0)->first();

            if ($mainMedia) {
                $mainMedia->main_image = 1;
                $mainMedia->save();
            }
        }

        $this->deleteFile($media->getOriginal('original_file_name'));
        $conversions = $media->conversions;

        foreach ($conversions as $k => $conversion) {
            $this->deleteFile($conversion['name']);
        }
    }

    /**
     * Удаление файла
     * @param string $name
     */
    private function deleteFile(string $name): void
    {
        $path = storage_path('app/public/' . config('media.upload_path')) . '/' . $name;
        if (file_exists($path)) {
            @unlink($path);
        }
    }

    /**
     * @param $img
     * @param array $conversions
     * @param string $originalFileName
     * @return array
     */
    private function saveInStore($img, array $conversions, string $originalFileName){
        // сохраняем оригинальное изображение
        $img->save($this->getUploadPath() . '/' . $originalFileName);

        // определяем конверсии. Берем из параметра или из конфига
        $conversions = (count($conversions)) ? $conversions : config('media.conversions');
        $converted = [];

        // ресайзим конверсии
        foreach ($conversions as $conversion => $size) {

            $img->resize($size, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $convertedName = $conversion . '_' . $originalFileName;

            $img->save($this->getUploadPath() . '/' . $convertedName);

            $converted[$conversion] = [
                'name' => $convertedName,
                'width' => $img->width(),
                'height' => $img->height(),
                'mime' => $img->mime()
            ];
        }

        return $converted;
    }

    /**
     * @param UploadedFile $image
     * @param array $conversions
     * @throws Exception
     */
    private function check(UploadedFile $image, array $conversions = []) {
        /**
         * Проверяем есть ли в конфиг файле переданные конверсии
         */
        if (count($conversions)) {
            foreach ($conversions as $conversion) {
                if (!in_array($conversion, config('media.conversions'))) {
                    throw new Exception('Conversion ' . $conversion . ' not present in config');
                }
            }
        }

        /**
         * Если изображение не валидно выбросим исклчение
         */
        if (!$image->isValid()) {
            throw new Exception('Argument $image is not valid media resource');
        }
    }

    /**
     * @return string
     */
    private function getUploadPath() {
        // определяем путь загрузки изображений
        $uploadPath = storage_path('app/public/' . config('media.upload_path'));


        if (!file_exists($uploadPath)) {
            mkdir($uploadPath, 0777,true);
        }

        return $uploadPath;
    }

}

